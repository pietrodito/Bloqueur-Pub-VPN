Voici mon aide-mémoire en français sur comment utiliser votre Raspberry Pi comme :

+ Un **bloqueur pub** qui protégera tout votre réseau local
  (avec [Pi-Hole](https://pi-hole.net/))
+ Un serveur qui vous permettra d'utiliser un **annuaire libre d'internet**
  luttant contre la [censure
  d'internet](https://fr.wikipedia.org/wiki/Censure_d'Internet_en_France)
+ Un **VPN** qui vous permettra de profiter de tout ceci même en dehors de chez
  vous et sur votre téléphone (avec [PiVPN](https://pivpn.io))
+ Un serveur **SSH** pour administer tout ceci à distance
+ Si votre FAI modifie votre adresse IP et que vous êtes loin de chez vous,
  une solution **sans DynDNS**
+ Et, comment utiliser un service VPN en même temps que votre bloqueur pub.

Les opérations suivantes sont plus faciles à suivre si vous utilisez une machine
Linux et encore plus si elle fait tourner Arch et Wayland.

____

Voici les différentes parties du guide :


- [Installation de Pi-Hole et PiVPN](#installation-de-pi-hole-et-pivpn)
  - [Télécharger Pi OS Lite](#tlcharger-pi-os-lite)
  - [Flasher une carte SD](#flasher-une-carte-sd)
  - [Préparer Pi OS pour une connexion ssh](#prparer-pi-os-pour-une-connexion-ssh)
  - [Se connecter au Pi](#se-connecter-au-pi)
  - [Installer Pi-Hole](#installer-pi-hole)
  - [Empêcher les connexions à l'interface web depuis l'extérieur](#empcher-les-connexions--linterface-web-depuis-lextrieur)
  - [Demander à Pi-Hole de devenir serveur DHCP](#demander--pi-hole-de-devenir-serveur-dhcp)
  - [Étendre les listes de blocage de Pi-Hole](#tendre-les-listes-de-blocage-de-pi-hole)
- [Installer PiVPN](#installer-pivpn)
- [Utiliser la 4G de son téléphone en profitant du bloqueur pub et des DNS libres](#utiliser-la-4g-de-son-tlphone-en-profitant-du-bloqueur-pub-et-des-dns-libres)
  - [Côté Pi](#ct-pi)
  - [Côté téléphone](#ct-tlphone)
  - [Tester l'utilisation des DNS - FDN](#tester-lutilisation-des-dns---fdn)
  - [Tester le blocage des pubs](#tester-le-blocage-des-pubs)
- [Utiliser un ordinateur portable en dehors de chez vous](#utiliser-un-ordinateur-portable-en-dehors-de-chez-vous)
  - [Côté Pi](#ct-pi)
  - [Installer wireguard sur l'ordinateur](#installer-wireguard-sur-lordinateur)
- [Utiliser un service VPN en même temps que le bloqueur pub](#utiliser-un-service-vpn-en-mme-temps-que-le-bloqueur-pub)
- [Contournement de l'IP changeante](#contournement-de-lip-changeante)
  - [Faire que le Pi publie l'IP publique sur un dépôt git dédié](#faire-que-le-pi-publie-lip-publique-sur-un-dpt-git-ddi)
  - [Se connecter depuis une machine distante](#se-connecter-depuis-une-machine-distante)

____

# Tunnel DoH pour résoudre les DNS sur le serveur de Bortzmever

+ uri du serveur DoH : https://doh.bortzmeyer.fr

Voici quelques éléments pour savoir si vous faîtes plus confiance à ce monsieur
plutôt qu'à Cloudflare, Google ou même Quad9 !

+ [Livre de Bortzmever](https://cyberstructure.fr/)
+ [Vidéo youtube de Bortzmever](https://www.youtube.com/watch?v=60JuRibWPEY)

## Télécharger Pi OS Lite

+ Copier le hash d'intégrité SHA256 puis lancer le torrent pour Pi OS Lite sur
  la [page de téléchargement de Pi
  OS](https://www.raspberrypi.com/software/operating-systems/)

+ Une fois que le fichier est téléchargé, on vérifie le SHA256 :

``` bash
## wl-paste ne marche qu'avec wayland et requiert le package wl-clipboard
[[ `sha256sum ~/Downloads/*raspios*lite*img.xz | awk '{print $1;}'` == `wl-paste` ]] && echo OK || echo ÉCHEC
```

## Flasher une carte SD

+ On télécharge Balena Etcher pour flasher la carte SD sur le
  [site](https://www.balena.io/etcher/)

+ Enfin on flashe la carte

## Préparer Pi OS pour une connexion ssh

Il faut ajouter deux fichiers pour permettre la connexion SSH :
  
+ `ssh`
+ `userconf` avec un nom d'utilisateur et un mot de passe cryté.

Voici comment faire :

Dans les lignes ci-dessous vous devez remplacer le nom d'utilisateur `ulys` par
le votre et renseigner un mot de passe pour vous connecter au Pi par SSH.

``` bash
touch ~/ssh
echo ulys:`echo 'mon_mot_de_passe' | openssl passwd -6 -stdin` > userconf
```

Puis copier ces deux fichiers sur la partition `Boot` de la carte SD

## Se connecter au Pi

Il faut d'abord se connecter à la box et créer un bail statique pour le
raspberrypi et noter l'adresse IP qui nous permettra d'accèder à l'interface
web du bloqueur pub.

Tout d'abord, nous allons copier notre clé SSH publique vers le Pi :

``` bash
ssh-copy-id -i ~/.ssh/id_rsa.pub ulys@raspberrypi
ssh ulys@raspberrypi ## Plus besoin de mot de passe
```

Puis sur le Pi, on fait une mise à jour :

``` bash
sudo apt update && sudo apt upgrade -y
```

## Installer et paramétrer cloudflared

On télécharge le package `.deb` sur le [site de
cloudflare](https://developers.cloudflare.com/cloudflare-one/connections/connect-apps/install-and-setup/installation#linux)

On le copie sur le RaspberryPi :

``` bash
 scp ~/Downloads/cloudflared-linux-arm64.deb ulys@raspberrypi:~
```

Puis enfin sur le RaspberryPi :

 ``` bash
sudo su
 ```

 ``` bash
dpkg -i ./cloudflared-linux-arm64.deb

echo 'CLOUDFLARED_OPTS=--port 5300 --upstream https://doh.bortzmeyer.fr' > /etc/default/cloudflared

echo '
[Unit]
Description=cloudflared DNS over HTTPS proxy
After=syslog.target network-online.target

[Service]
EnvironmentFile=/etc/default/cloudflared
ExecStart=/usr/local/bin/cloudflared proxy-dns $CLOUDFLARED_OPTS
Restart=on-failure
RestartSec=10
KillMode=process

[Install]
WantedBy=multi-user.target
 ' > /etc/systemd/system/cloudflared.service

systemctl enable cloudflared

systemctl start cloudflared
 ```
 
On teste si ça fonctionne, en quittant le root :

``` bash
sudo apt install -y dnsutils
dig @127.0.0.1 -p 5300 thepiratebay.org
```

Vous devez voir :

``` bash
...
;; ANSWER SECTION:
thepiratebay.org.       116     IN      A       162.159.137.6
thepiratebay.org.       116     IN      A       162.159.136.6
...
```

# Installation de Pi-Hole et PiVPN

## Installer Pi-Hole

``` bash
curl -sSL https://install.pi-hole.net | bash
```

Pour les paramètres DNS on choisit notre tunnel DoH :

+ `127.0.0.1#5300`

Je dis oui pour l'installation d'un serveur web pour l'administration et je
change les politiques sur la confidentialité en choisissant aucun log. Ceci pour
deux raisons, écomiser votre carte SD point faible du Pi, et ne pas espionner
les gens utilisant votre connexion. Respecter la vie privée, reste quand même le
but de toute cette manœuvre !

**Il faut bien noter son mot de passe administrateur**.

## Empêcher les connexions à l'interface web depuis l'extérieur

``` bash
pihole -a -i local
```

## Demander à Pi-Hole de devenir serveur DHCP

Dans l'interface d'administration choisir de se connecter (`Log in`) puis dans
paramètres/DHCP (`Settings`) il faut cocher `Enable DHCP Server` et `Enable IPv6
support` puis cliquer sur le bouton `Save` tout en bas.

Ensuite, vous devez désactiver sur votre box deux choses :
+ Le serveur DHCP
+ La connectivité IPv6


## Étendre les listes de blocage de Pi-Hole

+ Prendre toutes les listes vertes et bleues de https://firebog.net/
+ Ici une liste blanche d'autoristaion https://github.com/anudeepND/whitelist si
  les listes ci-dessus sont trop restrictives pour votre usage.

# Installer PiVPN

``` bash
curl -L https://install.pivpn.io | bash 
```

Nous allons choisir WireGuard plutôt qu'OpenVPN. Son seul défaut est de stocker
les IPs entrantes sur le serveur, mais pour notre usage privé cela nous importe
peu.

**PiVPN propose de choisir le port 51820 par défaut, je le remplace par 1194.**

Pour le DNS, je prends le CUSTOM et je choisis celui de mon routeur, soit `Use this public IP`.

Il faut également suivre ces recommendations `enable unattended-upgrades`

TODO rebooter le Pi tous les jours à 04:00

Il faut alors paramétrer la box pour ouvrir le port 1194 dans le menu NAT/PAT :

```
| Application/Service | Port interne | Port externe | Protocole | Équipement  | IP externe |
| PiVPN               | 1194         | 1194         | UDP       | raspberrypi | Toutes     |
```


# Utiliser la 4G de son téléphone en profitant du bloqueur pub et des DNS libres
## Côté Pi 

Il faut maintenant créer un profil client par appareil :

``` bash
pivpn add
```

On peut choisir pour nom de client `telephone_ulys`.

On génère ensuite un QR code sur Pi :

``` bash
pivpn -qr
```


## Côté téléphone

On installe l'application WireGuard sur son téléphone.

On ajoute le VPN en scannant le QR code obtenu ci-dessus.

## Tester l'utilisation des DNS - FDN

Vous pouvez par exemple aller sur [Sci-Hub](https://sci-hub.ru/) qui est
probablement banni par votre opérateur téléphone.

## Tester le blocage des pubs

Vous pouvez par exemple aller sur [slate.fr](http://www.slate.fr/), un site
*infotainement* de centre droit sur lequel je vais trop souvent et qui est
presque rendu illisible sur téléphone à cause des publicités...

# Utiliser un ordinateur portable en dehors de chez vous
## Côté Pi 

On crée un nouveau profil pour l'ordinateur

``` bash
pivpn add
```

On peut choisir pour nom de client `PC_portable`.

## Installer wireguard sur l'ordinateur

``` bash
## Pour arch
yay -S wireguard-tools
```

On copie ensuite le fichier de configuration du Pi vers le portable et on le
renomme `wg0.conf`.

``` bash
scp ulys@raspberrypi:~/configs/PC_portable.conf .
sudo mv ./PC_portable.conf /etc/wireguard/wg0.conf
```

On l'importe dans le NetworkManager :

``` bash
sudo nmcli connection import type wireguard file /etc/wireguard/wg0.conf
```

Désormais, lorsque l'on voudra utiliser le bloqueur pub depuis l'extérieur il suffira de faire :

``` bash
nmcli connection up wg0
```

Et pour cesser de l'utiliser :

``` bash
nmcli connection down wg0
```

# Utiliser un service VPN en même temps que le bloqueur pub

Cette fois vous devez être chez vous sur votre réseau local. Le but de cette
manœuvre est de pouvoir utiliser un service VPN, pour par exemple mimer un accès
à un site depuis un autre pays (par exemple,regarder un documentaire BBC depuis
le Royaume-Uni) sans perdre le confort du blocage des publicités. Un autre
avantage à utiliser un service VPN est de ne plus laisser votre ISP voir ce que
vous faites sur internet. Il vous reste cependant à faire confiance au service VPN.

Pour donner un exemple pratique je vous propose d'utiliser
[ProtonVPN](https://protonvpn.com/) basé en suisse et qui semble respecter la
vie privée.

Pour obtenir un compte il suffit juste de créer une adresse mail chez
[Proton.me](https://proton.me/) ce que je vous conseille aussi de faire si vous
êtes soucieux de votre vie privée.

Ensuite, on se rend sur la [page de configuration de WireGuard pour
ProtonVPN](https://account.protonvpn.com/downloads#wireguard-configuration) et
on télécharge un fichier `.conf`. 

Pour que votre connexion VPN passe par le bloqueur pub il faut modifier la ligne
suivante du fichier de config :

``` bash
DNS = 192.168.1.24 ## à remplacer par l'adresse statique du Pi
```

Ensuite vous devez importer le fichier avec NetworkManager et le brancher avec
la commande up comme plus haut. Ici, je nomme cette deuxième config `wg1` :

``` bash
sudo mv ./ProtonVPN_NL.conf /etc/wireguard/wg1.conf
sudo nmcli connection import type wireguard file /etc/wireguard/wg1.conf
nmcli connection up wg1
```

# Contournement de l'IP changeante

Tout ceci fonctionnera toujours en local chez vous. Mais sur vos appareils
connectés en dehors de chez vous, si votre FAI change l'adresse IP de votre box,
alors votre configuration sera brisée. Si vous êtes loin de chez vous, voici une
solution pour retrouver votre nouvelle IP sans service externe DynDNS.

## Faire que le Pi publie l'IP publique sur un dépôt git dédié

``` bash
sudo apt install git -y
```

Tout d'abord, créons un dépôt **privé** sur un hébergeur git. Par exemple,
[codeberg.org](https://codeberg.org/) pour ses valeurs éthiques.

Le dépôt s'appelle ici `mon-ip-publique`.

Maintenant générons une clé SSH sur le Pi et copions-la:

``` bash
ssh-keygen
cat ~/.ssh/id_rsa.pub
```

Puis on l'ajoute sur l'hébergeur git.

On clone alors le dépôt sur le Pi attention à bien utiliser le lien SSH.

``` bash
git clone git@codeberg.org:pietrodito/mon-ip-publique.git ~/mon-ip-publique
touch ~/mon-ip-publique/adresse-ip
cd ~/mon-ip-publique
git add adresse-ip
git config --global user.email "pierre [dot] balaye [at] proton [dot] me"
git config --global user.name  "Pierre Balaye"
git commit -m 'Premier engagement'
```

L'idée est de pousser chaque heure l'adresse IP sur le dépôt git.

Pour cela on crée un script capable de le faire :
```bash
## Fichier: ~/envoie-ip-vers-depot-distant.sh
wget -O ~/mon-ip-publique/adresse-ip icanhazip.com
git -C ~/mon-ip-publique commit -am 'Nouvelle ip'
git -C ~/mon-ip-publique push
```

Vous devez rendre ce script executable :

``` bash
chmod +x ~/envoie-ip-vers-depot-distant.sh
```

Enfin on crée un job crontab pour publier chaque heure :

``` bash
echo "00 * * * * ~/envoie-ip-vers-depot-distant.sh" >> mycron
crontab mycron
rm mycron
```

## Se connecter depuis une machine distante

Tout d'abord sur le Pi il faut empècher les connexions à distance avec mot de
passe. Vous ne pourrez plus vous connecter qu'avec votre clé SSH :

``` bash
sudo sed -i 's/PasswordAuthentication yes/PasswordAuthentication no/' /etc/ssh/sshd_config 
```

Puis vous pouvez paramétrer la box pour ouvrir le port 22 dans le menu NAT/PAT :

```
| Application/Service | Port interne | Port externe | Protocole | Équipement  | IP externe |
| ssh                 | 22           | 22           | TCP       | raspberrypi | Toutes     |
```


Maintenant sur la machine qui doit venir se connecter sur le Pi depuis
l'extérieur de votre réseau.

``` bash
git clone git@codeberg.org:pietrodito/mon-ip-publique.git ~/Comp/mon-ip-publique
```

Voici le script pour se connecter au Pi à distance à placer dans le `$PATH` :

``` bash
## ~/bin/ssh-pi-distant.sh
## Connexion à distance
git -C ~/Comp/mon-ip-publique pull
ssh ulys@`cat ~/Comp/mon-ip-publique/adresse-ip`
```
